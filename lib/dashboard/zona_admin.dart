import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:material_kit_flutter/services/crud_services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import '../constants/Theme.dart';

class Zonas extends StatefulWidget {
  const Zonas({Key key}) : super(key: key);

  @override
  State<Zonas> createState() => _ZonasState();
}

class _ZonasState extends State<Zonas> {
  // Query dbRef = FirebaseDatabase.instance.ref('Zonas');

  Stream streamList =
      FirebaseFirestore.instance.collection('Zonas').snapshots();
  Widget listItem({Map pdf}) {
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      height: 110,
      color: Color.fromARGB(255, 219, 219, 219),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              GestureDetector(
                child: Image(
                  image: AssetImage('assets/img/lugar.png'),
                  width: 50.0,
                  height: 50.0,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(
                      pdf['Zona'],
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: StreamBuilder<String>(
                      stream:
                          CRUDServices().getNombreciudadStream(pdf['Ciudad']),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data.length > 0) {
                            return Text(
                              snapshot.data,
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.normal),
                            );
                          } else {
                            return CircularProgressIndicator();
                          }
                        } else {
                          return Text(
                            '-',
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.normal),
                          );
                        }
                      },
                    ),
                  )
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/UpdateZona',
                      arguments: pdf['key']);
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.edit,
                      color: Colors.black,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 6,
              ),
              GestureDetector(
                onTap: () async {
                  bool result = await showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) {
                      return AlertDialog(
                        title: Text('¿Esta seguro de eliminar esta zona?'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context, false);
                            },
                            child: Text('No'),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context, true);
                            },
                            child: Text('Si'),
                          ),
                        ],
                      );
                    },
                  );
                  if (result) {
                    final resp =
                        await CRUDServices().deleteData('Zonas', pdf['key']);
                    if (resp) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Zona eliminado correctamente'),
                        backgroundColor: Colors.green,
                      ));
                    } else {
                      print('Ocurrio un error');
                    }
                  }
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.delete,
                      color: Colors.red[700],
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SpeedDial(
            backgroundColor: MaterialColors.myprimary,
            animatedIcon: AnimatedIcons.menu_close,
            children: [
              SpeedDialChild(
                  child: Icon(Icons.add),
                  onTap: (() => Navigator.pushNamed(context, '/toAddZona')),
                  label: "Añadir Zona",
                  backgroundColor: Colors.lightGreen),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          FloatingActionButton(
            child: Icon(
              Icons.arrow_back_sharp,
              color: Colors.white,
            ),
            backgroundColor: MaterialColors.myprimary,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      appBar: AppBar(
        title: const Text("Zonas"),
        backgroundColor: MaterialColors.myprimary,
      ),
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      // key: _scaffoldKey,

      body: Container(
          height: double.infinity,
          child: StreamBuilder(
            stream: streamList,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text("Ocurrio un error"),
                    )
                  ],
                );
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text("Cargando datos"),
                    )
                  ],
                );
              }
              QuerySnapshot query = snapshot.data;
              List<Widget> list = [];
              for (var document in query.docs) {
                if (document.exists) {
                  Map pdf = document.data();
                  pdf['key'] = document.id;
                  list.add(listItem(pdf: pdf));
                }
              }

              return ListView(children: list);
            },
          )
          //  FirebaseAnimatedList(
          //   query: dbRef,
          //   itemBuilder: (BuildContext context, DataSnapshot snapshot,
          //       Animation<double> animation, int index) {
          //     print(snapshot.value);
          //     Map pdf = snapshot.value as Map;
          //     pdf['key'] = snapshot.key;

          //     return listItem(pdf: pdf);
          //   },
          // ),
          ),
    );
  }
}
