import 'package:flutter/material.dart';
import 'package:material_kit_flutter/constants/Theme.dart';
import 'package:material_kit_flutter/services/crud_services.dart';

class AddResiduo extends StatefulWidget {
  const AddResiduo({Key key}) : super(key: key);

  @override
  State<AddResiduo> createState() => _AddResiduos();
}

class _AddResiduos extends State<AddResiduo> {
  final TextEditingController _nombreCiudadController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Añadir Residuo"),
        backgroundColor: MaterialColors.myprimary,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Form(
            //que el boton vaya al fornmulario de recicladora
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                    controller: _nombreCiudadController,
                    decoration: const InputDecoration(
                      icon: Icon(
                        Icons.music_video,
                        color: MaterialColors.mysecondary,
                      ),
                      helperText: 'Nombre del residuo',
                      labelText: 'Ingrese el Nombre del residuo',
                      labelStyle: TextStyle(color: MaterialColors.mysecondary),
                      enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: MaterialColors.mysecondary)),
                      focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: MaterialColors.mysecondary)),
                      border: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: MaterialColors.mysecondary)),
                    ),
                    validator: ((value) => (value))),
                SizedBox(
                  height: 15.0,
                ),
                SizedBox(
                  height: 15.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Center(
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: MaterialColors.mysecondary,
                        minimumSize: const Size(180.0, 40.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      onPressed: () async {
                        bool resp = await CRUDServices().guardarDatapdf(
                            {'nombre': _nombreCiudadController.text},
                            "Residuos");
                        if (resp) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Residuo registrada'),
                            backgroundColor: Colors.green,
                          ));
                          Navigator.pop(context);
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Algo salio mal'),
                            backgroundColor: Colors.red,
                          ));
                        }
                      },
                      child: Text('Guardar',
                          style: TextStyle(color: Colors.white))),
                )
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      // key: _scaffoldKey,
    );
  }
}
