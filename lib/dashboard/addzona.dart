import 'package:flutter/material.dart';
import 'package:material_kit_flutter/constants/Theme.dart';
import 'package:material_kit_flutter/services/crud_services.dart';

import '../model/ciudad_model.dart';

class AddZona extends StatefulWidget {
  const AddZona({Key key}) : super(key: key);

  @override
  State<AddZona> createState() => _AddZonaState();
}

class _AddZonaState extends State<AddZona> {
  List<Ciudad> itemCiudades = [];
  final _listacopioservice = CRUDServices();
  Ciudad comboCiudad;
  final TextEditingController _zonaCiudadController = TextEditingController();

  Widget _elegirCiudad() {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          padding: EdgeInsets.only(left: 28.0),
          margin: EdgeInsets.only(left: 13.0, right: 0),
          child: DropdownButtonFormField(
            isExpanded: true,
            hint: Text(
              'Elegir ciudad',
              style: TextStyle(color: MaterialColors.mysecondary),
            ),
            items: itemCiudades.map((valueItem) {
              return DropdownMenuItem(
                value: valueItem,
                child: Text(valueItem.nombre),
              );
            }).toList(),
            value: comboCiudad,
            onChanged: (value) {
              setState(() {
                comboCiudad = value;
              });
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 13.0,
          ),
          //margin: EdgeInsets.only(top: 80.0, left: 28.0),
          child: Icon(
            Icons.place,
            color: MaterialColors.mysecondary,
            size: 25.0,
          ),
        ),
      ],
    );
  }

  void initvalues() {
    _listacopioservice.getciudades().then((value) {
      setState(() {
        itemCiudades = value;
      });
    });
  }

  @override
  initState() {
    super.initState();
    initvalues();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Añadir zona"),
        backgroundColor: MaterialColors.myprimary,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Form(
            //que el boton vaya al fornmulario de recicladora
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _elegirCiudad(),
                SizedBox(
                  height: 15.0,
                ),
                TextFormField(
                    controller: _zonaCiudadController,
                    decoration: const InputDecoration(
                      icon: Icon(
                        Icons.link,
                        color: MaterialColors.mysecondary,
                      ),
                      helperText: 'Zona de la ciudad',
                      labelText: 'Ingrese la zona de una ciudad',
                      labelStyle: TextStyle(color: MaterialColors.mysecondary),
                      enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: MaterialColors.mysecondary)),
                      focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: MaterialColors.mysecondary)),
                      border: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: MaterialColors.mysecondary)),
                    ),
                    validator: ((value) => (value))),
                SizedBox(
                  height: 15.0,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Center(
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: MaterialColors.mysecondary,
                        minimumSize: const Size(180.0, 40.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      onPressed: () async {
                        bool resp = await CRUDServices().guardarDatapdf({
                          'Zona': _zonaCiudadController.text,
                          'Ciudad': comboCiudad.key
                        }, "Zonas");
                        if (resp) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Ciudad y zona registrada'),
                            backgroundColor: Colors.green,
                          ));
                          Navigator.pop(context);
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text('Algo salio mal'),
                            backgroundColor: Colors.red,
                          ));
                        }
                      },
                      child: Text('Guardar',
                          style: TextStyle(color: Colors.white))),
                )
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      // key: _scaffoldKey,
    );
  }
}
