import 'package:firebase_auth/firebase_auth.dart';
import '../dashboard_styles/color.dart';
import '../dashboard_styles/typo.dart';
import 'package:flutter/material.dart';

class GetStarted extends StatelessWidget {
  Future logout() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green[300],
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 10,
            ),
            Image.asset('assets/img/REDcicla.png', height: 150),
            SizedBox(
              height: 10,
            ),
            Text(
              'Perfil del administrador',
              style: header,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() =>
                                    Navigator.pushNamed(context, '/pdfadmin')),
                                child: Image.asset('assets/img/pdf.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'PDFs',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() =>
                                    Navigator.pushNamed(context, '/useradmin')),
                                child: Image.asset(
                                    'assets/img/profile-user.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Usuarios',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() => Navigator.pushNamed(
                                    context, '/videoadmin')),
                                child: Image.asset('assets/img/youtubeplay.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Videos',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() => Navigator.pushNamed(
                                    context, '/Ciudadesadmin')),
                                child: Image.asset('assets/img/ciudad.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Ciudades',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() => Navigator.pushNamed(
                                    context, '/Zonasadmin')),
                                child: Image.asset('assets/img/lugar.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Zonas',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() => Navigator.pushNamed(
                                    context, '/Residuosadmin')),
                                child: Image.asset('assets/img/residuos.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Residuos',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                          ),
                          padding: EdgeInsets.symmetric(vertical: 16),
                          width: double.infinity,
                          child: Column(
                            children: [
                              InkWell(
                                onTap: (() => logout()),
                                child: Image.asset(
                                    'assets/img/cerrar sesion.png',
                                    height: 50),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'Cerrar sesión',
                                style: subHeader,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
