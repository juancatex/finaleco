import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import '../constants/Theme.dart';
import '../model/global_model.dart';
import '../screens/form_ecoemprendimiento.dart';
import '../screens/form_recicladora.dart';
import '../services/crud_services.dart';

class Users extends StatefulWidget {
  const Users({Key key}) : super(key: key);
  @override
  State<Users> createState() => _UsersState();
}

class _UsersState extends State<Users> {
  Stream<List<Globalmodel>> listaglobalstream;
  final _listacopioservice = CRUDServices();
  void initvalues() {
    listaglobalstream = _listacopioservice.getglobalvaluesStream('');
  }

  @override
  void initState() {
    super.initState();
    initvalues();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: SpeedDial(
          backgroundColor: MaterialColors.myprimary,
          overlayColor: Colors.lightGreen,
          overlayOpacity: 0.6,
          animatedIcon: AnimatedIcons.menu_close,
          children: [
            SpeedDialChild(
                child: Icon(Icons.add),
                label: "Añadir Ecoemprendimiento",
                onTap: () async {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RegisterEcoemprendimiento(
                            userindata: MaterialColors.getnewuser())),
                  );
                  setState(() {
                    listaglobalstream =
                        _listacopioservice.getglobalvaluesStream('');
                  });
                }),
            SpeedDialChild(
                child: Icon(Icons.add),
                label: "Añadir Recicladora",
                onTap: () async {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RegisterRecicladora(
                            userindata: MaterialColors.getnewuser())),
                  );
                  setState(() {
                    listaglobalstream =
                        _listacopioservice.getglobalvaluesStream('');
                  });
                }),
          ],
        ),
        appBar: AppBar(
          title: const Text("Lista de usuarios"),
          backgroundColor: MaterialColors.myprimary,
        ),
        body: Column(
          children: [
            StreamBuilder<List<Globalmodel>>(
              stream: listaglobalstream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    padding: EdgeInsets.all(8),
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      final item = snapshot.data[index];

                      return Card(
                        child: ListTile(
                          iconColor: Colors.red,
                          title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  item.nombre + " - " + item.rol,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  item.email,
                                  style: TextStyle(fontSize: 12),
                                )
                              ]),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                  color: Colors.black,
                                  onPressed: () {
                                    switch (item.rol) {
                                      case 'Ecoemprendedor':
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  RegisterEcoemprendimiento(
                                                      userindata: item)),
                                        );
                                        break;
                                      case 'Reciclador':
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  RegisterRecicladora(
                                                      userindata: item)),
                                        );
                                        break;
                                    }
                                  },
                                  icon: const Icon(Icons.edit)),
                              IconButton(
                                  onPressed: () async {
                                    bool result = await showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text(
                                              '¿Esta seguro de eliminar el registro del usuario?'),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(context, false);
                                              },
                                              child: Text('No'),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(context, true);
                                              },
                                              child: Text('Si'),
                                            ),
                                          ],
                                        );
                                      },
                                    );

                                    if (result) {
                                      final resp = await CRUDServices()
                                          .deleteData('registro', item.uid);
                                      if (resp) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(const SnackBar(
                                          content: Text(
                                              'Usuario eliminado correctamente'),
                                          backgroundColor: Colors.green,
                                        ));
                                        setState(() {
                                          listaglobalstream = _listacopioservice
                                              .getglobalvaluesStream('');
                                        });
                                      } else {
                                        print('Ocurrio un error');
                                      }
                                    }
                                  },
                                  icon: const Icon(Icons.delete)),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }
                return Center(child: CircularProgressIndicator());
              },
            )
          ],
        ));
  }
}
