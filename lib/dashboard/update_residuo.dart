import 'package:flutter/material.dart';
import 'package:material_kit_flutter/services/crud_services.dart';

import '../constants/Theme.dart';

class UpdateResiduo extends StatefulWidget {
  const UpdateResiduo({Key key}) : super(key: key);

  @override
  State<UpdateResiduo> createState() => _UpdateResiduo();
}

class _UpdateResiduo extends State<UpdateResiduo> {
  final String entidad = 'Residuos';
  final residuoNombreController = TextEditingController();
  Map dataPdf;
  void getPdfData(String pdfKey) async {
    dataPdf = await CRUDServices().getOneData(entidad, pdfKey);
    residuoNombreController.text = dataPdf['nombre'];
  }

  @override
  Widget build(BuildContext context) {
    final String pdfKey = ModalRoute.of(context).settings.arguments as String;
    getPdfData(pdfKey);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Actualizar registro"),
        backgroundColor: MaterialColors.myprimary,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              const Text(
                'Actualizando registro',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 30,
              ),
              TextField(
                controller: residuoNombreController,
                keyboardType: TextInputType.text,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  helperText: 'Ingrese el nombre del residuo',
                  labelText: 'Título del residuo',
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: MaterialColors.mysecondary,
                    minimumSize: const Size(180.0, 40.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                  ),
                  onPressed: () async {
                    bool resp = await CRUDServices().modificarData(
                        {'nombre': residuoNombreController.text},
                        entidad,
                        pdfKey);
                    if (resp) {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Residuo actualizado'),
                        backgroundColor: Colors.green,
                      ));
                      Navigator.pop(context);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Algo salio mal'),
                        backgroundColor: Colors.red,
                      ));
                    }
                  },
                  child: Text('Actualizar Datos',
                      style: TextStyle(color: Colors.white)))
            ],
          ),
        ),
      ),
    );
  }
}
