import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';
import 'package:time_range_picker/time_range_picker.dart';

import '../model/global_model.dart';

class MaterialColors {
  static const Color defaultButton = Color.fromRGBO(220, 220, 220, 1.0);
  static const Color primary = Color.fromRGBO(156, 38, 176, 1.0);
  static const Color label = Color.fromRGBO(254, 36, 114, 1.0);
  static const Color info = Color.fromRGBO(0, 188, 212, 1.0);
  static const Color error = Color.fromRGBO(244, 67, 54, 1.0);
  static const Color success = Color.fromRGBO(76, 175, 80, 1.0);
  static const Color warning = Color.fromRGBO(255, 152, 0, 1.0);
  static const Color muted = Color.fromRGBO(151, 151, 151, 1.0);
  static const Color input = Color.fromRGBO(220, 220, 220, 1.0);
  static const Color active = Color.fromRGBO(156, 38, 176, 1.0);
  static const Color placeholder = Color.fromRGBO(159, 165, 170, 1.0);
  static const Color switch_off = Color.fromRGBO(212, 217, 221, 1.0);
  static const Color gradientStart = Color.fromRGBO(107, 36, 170, 1.0);
  static const Color gradientEnd = Color.fromRGBO(172, 38, 136, 1.0);
  static const Color priceColor = Color.fromRGBO(234, 213, 251, 1.0);
  static const Color border = Color.fromRGBO(231, 231, 231, 1.0);
  static const Color caption = Color.fromRGBO(74, 74, 74, 1.0);
  static const Color bgColorScreen = Color.fromRGBO(238, 238, 238, 1.0);
  static const Color drawerHeader = Color.fromRGBO(75, 25, 88, 1.0);
  static const Color signStartGradient = Color.fromRGBO(108, 36, 170, 1.0);
  static const Color signEndGradient = Color.fromRGBO(21, 0, 43, 1.0);
  static const Color socialFacebook = Color.fromRGBO(59, 89, 152, 1.0);
  static const Color socialTwitter = Color.fromRGBO(91, 192, 222, 1.0);
  static const Color socialDribbble = Color.fromRGBO(234, 76, 137, 1.0);

  /* Mis colores */
  static const Color myprimary = Color.fromARGB(255, 20, 153, 93);
  static const Color mysecondary = Color.fromARGB(255, 20, 153, 93);

  static Future<TextEditingValue> OntapinputfieldTimer(
      TextEditingController _horarioAtencionController,
      BuildContext context) async {
    List starthora = [];
    List endhora = [];
    List timerinit =
        _horarioAtencionController.text.trim().toString().split('-');

    if (timerinit.length > 1) {
      starthora = timerinit[0].toString().split(':');
      endhora = timerinit[1].toString().split(':');
    }
    TimeRange result = await showTimeRangePicker(
        hideButtons: false,
        strokeColor: MaterialColors.myprimary,
        handlerColor: MaterialColors.myprimary,
        context: context,
        start: timerinit.length > 1
            ? TimeOfDay(
                hour: int.parse(starthora[0].toString()),
                minute: int.parse(starthora[1].toString()))
            : const TimeOfDay(hour: 4, minute: 0),
        end: timerinit.length > 1
            ? TimeOfDay(
                hour: int.parse(endhora[0].toString()),
                minute: int.parse(endhora[1].toString()))
            : const TimeOfDay(hour: 23, minute: 0),
        disabledTime: TimeRange(
            startTime: const TimeOfDay(hour: 23, minute: 0),
            endTime: const TimeOfDay(hour: 4, minute: 0)),
        disabledColor: Colors.red.withOpacity(0.5),
        strokeWidth: 4,
        ticks: 24,
        ticksOffset: -7,
        ticksLength: 15,
        ticksColor: Colors.grey,
        labels: [
          "12 am",
          "3 am",
          "6 am",
          "9 am",
          "12 pm",
          "3 pm",
          "6 pm",
          "9 pm"
        ].asMap().entries.map((e) {
          return ClockLabel.fromIndex(idx: e.key, length: 8, text: e.value);
        }).toList(),
        labelOffset: 35,
        fromText: "Desde",
        toText: "Hasta",
        interval: const Duration(minutes: 30),
        rotateLabels: false,
        padding: 60);

    return TextEditingValue(
        text: result != null
            ? (result.startTime.hour.toString().padLeft(2, "0") +
                ':' +
                result.startTime.minute.toString().padLeft(2, "0") +
                '-' +
                result.endTime.hour.toString().padLeft(2, "0") +
                ':' +
                result.endTime.minute.toString().padLeft(2, "0"))
            : '');
  }

  static Future openmodalResiduos(
      TextEditingController textfieldin, BuildContext context, modl) async {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: new Row(
            children: [
              CircularProgressIndicator(),
              Container(
                  margin: EdgeInsets.only(left: 17),
                  child: Text("Obteniendo datos...")),
            ],
          ),
        );
      },
    );
    List listtexts = textfieldin.text.trim().toString().length > 0
        ? textfieldin.text.trim().toString().split(', ')
        : [];
    List<MultiSelectItem> reciduoss = [];

    QuerySnapshot event =
        await FirebaseFirestore.instance.collection('Residuos').get();

    for (var element in event.docs) {
      Map value = element.data();
      reciduoss.add(MultiSelectItem(value['nombre'], value['nombre']));
    }

    Navigator.pop(context);
    await showDialog(
      context: context,
      builder: (ctx) {
        return MultiSelectDialog(
          searchHint: "Buscar residuo",
          searchable: true,
          title: Text("Residuos"),
          items: reciduoss,
          initialValue: listtexts,
          onConfirm: (values) {
            modl(values.join(', '));
          },
        );
      },
    );
  }

  static Future openmodalDiasSemana(
      TextEditingController textfieldin, BuildContext context, modl) async {
    List listtexts = textfieldin.text.trim().toString().length > 0
        ? textfieldin.text.trim().toString().split(', ')
        : [];
    List<MultiSelectItem> diassem = [];
    diassem.add(MultiSelectItem('Lunes', 'Lunes'));
    diassem.add(MultiSelectItem('Martes', 'Martes'));
    diassem.add(MultiSelectItem('Miercoles', 'Miercoles'));
    diassem.add(MultiSelectItem('Jueves', 'Jueves'));
    diassem.add(MultiSelectItem('Viernes', 'Viernes'));
    diassem.add(MultiSelectItem('Sabado', 'Sabado'));
    diassem.add(MultiSelectItem('Domingo', 'Domingo'));

    await showDialog(
      context: context,
      builder: (ctx) {
        return MultiSelectDialog(
          title: Text("Dias de la semana"),
          items: diassem,
          initialValue: listtexts,
          onConfirm: (values) {
            modl(values.join(', '));
          },
        );
      },
    );
  }

  static Stack Fondomodalcard() {
    final cajaLogo2 = Transform.rotate(
        angle: -3.1415926535897932 / 6.0,
        child: Stack(
          children: <Widget>[
            Container(
                height: 250.0,
                width: 350.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(85),
                  gradient: LinearGradient(
                    colors: [Colors.white, Colors.white],
                  ),
                )),
            Positioned(
              top: 25.0,
              right: 15,
              child: Transform.rotate(
                angle: 3.1415926535897932 / 6,
                child: Image(
                  image: AssetImage('assets/img/iniciobanner.jpg'),
                  height: 35,
                ),
              ),
            )
          ],
        ));
    final cajaBlanca = Transform.rotate(
      angle: -3.1415926535897932 / 6.0,
      child: Container(
        height: 280,
        width: 380,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(85),
            gradient: LinearGradient(colors: [Colors.white, Colors.white])),
      ),
    );
    return Stack(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: FractionalOffset(0.0, 0.5),
                  end: FractionalOffset(0.0, 1.0),
                  colors: [Color.fromRGBO(34, 181, 115, 1.0), Colors.green])),
        ),
        Positioned(top: -260.0, child: cajaBlanca),
        Positioned(bottom: -230.0, child: cajaLogo2)
      ],
    );
  }

  static Globalmodel getnewuser() {
    var rng = new Random();
    var name = rng.nextInt(100000);
    String nameuser = 'user_' + name.toString();
    String nameuseremail = nameuser + '@gmail.com';

    var r = Random();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    String keyrandomuid =
        List.generate(28, (index) => _chars[r.nextInt(_chars.length)]).join();

    return Globalmodel(
        nombre: nameuser, uid: keyrandomuid, email: nameuseremail);
  }
}
