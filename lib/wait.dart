import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'constants/Theme.dart';
import 'dashboard/getstarted.dart';
import 'screens/home.dart';

class Wait extends StatefulWidget {
  const Wait({Key key}) : super(key: key);

  @override
  State<Wait> createState() => _WaitState();
}

class _WaitState extends State<Wait> {
  bool paso = true;
  Map userin;
  Future initdata() async {
    var user = FirebaseAuth.instance.currentUser;
    var datadb = await FirebaseFirestore.instance
        .collection('registro')
        .doc(user.uid)
        .get();

    if (datadb.exists) {
      setState(() {
        userin = datadb.data();
      });
    } else {
      setState(() {
        userin = null;
      });
    }
    setState(() {
      paso = !paso;
    });
  }

  Widget waiting() {
    return Material(
        type: MaterialType.transparency,
        child: Container(
            color: MaterialColors.mysecondary,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 18),
                  child: Text(
                    'Cargando datos...',
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                )
              ],
            )));
  }

  @override
  void initState() {
    super.initState();
    initdata();
  }

  @override
  Widget build(BuildContext context) {
    return paso
        ? waiting()
        : (userin != null
            ? ((userin['rol'] == 'Admin')
                ? GetStarted()
                : HomeInicio(usermap: userin))
            : HomeInicio());
  }
}
