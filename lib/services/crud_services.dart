import 'package:cloud_firestore/cloud_firestore.dart';
import '../model/acopio_model.dart';
import '../model/ciudad_model.dart';
import '../model/eco_model.dart';
import '../model/global_model.dart';
import '../model/recicladora_model.dart';

import '../model/zona_model.dart';

class CRUDServices {
  Future<bool> guardarData(Object data, String table, String key) async {
    try {
      await FirebaseFirestore.instance.collection(table).doc(key).set(data);
      // FirebaseDatabase.instance.ref('$table').set(data);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> guardarDatapdf(Object data, String table) async {
    try {
      await FirebaseFirestore.instance.collection(table).add(data);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> modificarData(Object data, String table, String doc) async {
    try {
      await FirebaseFirestore.instance.collection(table).doc(doc).update(data);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<Map> getOneData(String table, key) async {
    try {
      DocumentSnapshot getdatain =
          await FirebaseFirestore.instance.collection(table).doc(key).get();
      return getdatain.data();
    } catch (e) {
      print(e);
      return {};
    }
  }

  Future<bool> deleteData(String table, key) async {
    try {
      await FirebaseFirestore.instance.collection(table).doc(key).delete();
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> deleteCiudad(String table, keyin) async {
    try {
      QuerySnapshot event = await FirebaseFirestore.instance
          .collection('Zonas')
          .where("Ciudad", isEqualTo: keyin)
          .get();

      for (var doczona in event.docs) {
        await doczona.reference.delete();
      }
      await FirebaseFirestore.instance.collection(table).doc(keyin).delete();
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Stream<List<Globalmodel>> getglobalvaluesStream(String nom) async* {
    List<Globalmodel> recicladoras = [];
    try {
      QuerySnapshot event =
          await FirebaseFirestore.instance.collection('registro').get();

      for (var element in event.docs) {
        Map value = element.data();
        if (value['rol'] != 'Admin') {
          recicladoras.add(new Globalmodel(
              nombre: value['nombre'],
              rol: value['rol'],
              uid: element.id,
              email: value['correo'],
              values: value));
        }
      }

      if (nom != null && nom.isNotEmpty) {
        recicladoras.removeWhere((value) => !value.values['residuos']
            .toString()
            .toLowerCase()
            .contains(nom.toLowerCase()));
      }
      yield recicladoras;
    } catch (e) {
      print('------------------------eeeeee---------------------------');
      print(e);
      yield recicladoras;
    }
  }

  Stream<List<Recicladora>> getRecicladoraStream(
      String nom, Ciudad ciudad, Zona zona) async* {
    List<Recicladora> recicladoras = [];
    try {
      yield recicladoras;
      QuerySnapshot event =
          await FirebaseFirestore.instance.collection('registro').get();

      for (var element in event.docs) {
        Map value = element.data();
        if (value['rol'] == 'Reciclador') {
          print('entrooooooooooooooooooooooooooo');
          String zonain = await getuniquezona(value['zona']);
          String cuidadin = await getuniqueciudad(value['departamento']);
          recicladoras.add(Recicladora(
              value['nombre'],
              value['uidUser'],
              cuidadin,
              zonain,
              value['celular'],
              value['correo'],
              value['dias'],
              value['horarios'],
              value['residuos'],
              value['ruta'],
              value['detalles']));
        }
      }

      print(recicladoras);

      if (nom != null && nom.isNotEmpty) {
        recicladoras.removeWhere(
            (value) => !value.nombre.toLowerCase().contains(nom.toLowerCase()));
      }
      if (ciudad != null &&
          ciudad.nombre.isNotEmpty &&
          ciudad.nombre != "Todas") {
        recicladoras.removeWhere((value) => !value.departamento
            .toLowerCase()
            .contains(ciudad.nombre.toLowerCase()));
      }
      if (zona != null && zona.nombre.isNotEmpty && zona.nombre != "Todas") {
        recicladoras.removeWhere((value) =>
            !value.zona.toLowerCase().contains(zona.nombre.toLowerCase()));
      }

      yield recicladoras.length > 0 ? recicladoras : null;
    } catch (e) {
      print('------------------------eeeeee---------------------------');
      print(e);
      yield null;
    }
  }

  Stream<List<Acopio>> getAcopioStream(
      String nom, Ciudad ciudad, Zona zona) async* {
    List<Acopio> acopiadoras = [];
    try {
      yield acopiadoras;
      QuerySnapshot event =
          await FirebaseFirestore.instance.collection('registro').get();
      for (var element in event.docs) {
        Map value = element.data();
        if (value['rol'] == 'Acopiador') {
          String zonain = await getuniquezona(value['zona']);
          String cuidadin = await getuniqueciudad(value['ciudad']);
          acopiadoras.add(new Acopio(
              value['nombre'],
              value['uidUser'],
              cuidadin,
              zonain,
              value['celular'],
              value['correo'],
              value['horarios'],
              value['direccion'],
              value['residuos'],
              value['detalles']));
        }
      }
      if (nom != null && nom.isNotEmpty) {
        if (acopiadoras.isNotEmpty)
          acopiadoras.removeWhere((value) =>
              !value.nombre.toLowerCase().contains(nom.toLowerCase()));
      }
      if (ciudad != null &&
          ciudad.nombre.isNotEmpty &&
          ciudad.nombre != "Todas") {
        acopiadoras.removeWhere((value) =>
            !value.ciudad.toLowerCase().contains(ciudad.nombre.toLowerCase()));
      }
      if (zona != null && zona.nombre.isNotEmpty && zona.nombre != "Todas") {
        acopiadoras.removeWhere((value) =>
            !value.zona.toLowerCase().contains(zona.nombre.toLowerCase()));
      }

      yield acopiadoras.length > 0 ? acopiadoras : null;
    } catch (e) {
      print('------------------------eeeeeee---------------------------');
      print(e);
      yield null;
    }
  }

  Future<List<Ecoemprendedor>> ecoemprendedor() async {
    QuerySnapshot event =
        await FirebaseFirestore.instance.collection('registro').get();
    List<Ecoemprendedor> recicladoras = [];

    for (var element in event.docs) {
      Map value = element.data();
      if (value['rol'] == 'ecoemprendedor') {
        recicladoras.add(new Ecoemprendedor(
            value['nombre'],
            value['uidUser'],
            value['ciudad'],
            value['zona'],
            value['celular'],
            value['correo'],
            value['residuos'],
            value['horarios'],
            value['capacidad'],
            value['detalles'],
            value['direccion'],
            value['descripcion']));
      }
    }

    return recicladoras;
  }

  Stream<List<Ecoemprendedor>> ecoemprendedorStream(
      String nom, Ciudad ciudad, Zona zona) async* {
    List<Ecoemprendedor> ecoemprendedor = [];
    try {
      yield ecoemprendedor;
      QuerySnapshot event =
          await FirebaseFirestore.instance.collection('registro').get();

      for (var element in event.docs) {
        Map value = element.data();
        if (value['rol'] == 'Ecoemprendedor') {
          String zonain = await getuniquezona(value['zona']);
          String cuidadin = await getuniqueciudad(value['ciudad']);
          ecoemprendedor.add(new Ecoemprendedor(
              value['nombre'],
              value['uidUser'],
              cuidadin,
              zonain,
              value['celular'],
              value['correo'],
              value['residuos'],
              value['horarios'],
              value['capacidad'],
              value['detalles'],
              value['direccion'],
              value['descripcion']));
        }
      }

      if (nom != null && nom.isNotEmpty) {
        ecoemprendedor.removeWhere(
            (value) => !value.nombre.toLowerCase().contains(nom.toLowerCase()));
      }
      if (ciudad != null &&
          ciudad.nombre.isNotEmpty &&
          ciudad.nombre != "Todas") {
        ecoemprendedor.removeWhere((value) =>
            !value.ciudad.toLowerCase().contains(ciudad.nombre.toLowerCase()));
      }
      if (zona != null && zona.nombre.isNotEmpty && zona.nombre != "Todas") {
        ecoemprendedor.removeWhere((value) =>
            !value.zona.toLowerCase().contains(zona.nombre.toLowerCase()));
      }
      yield ecoemprendedor.length > 0 ? ecoemprendedor : null;
    } catch (e) {
      print('------------------------eeeeee---------------------------');
      print(e);
      yield null;
    }
  }

  Future<Map> esValueRol(String userUid) async {
    DocumentSnapshot getdatain = await FirebaseFirestore.instance
        .collection('registro')
        .doc(userUid)
        .get();
    return getdatain.exists ? getdatain.data() : {};
  }

  Future<List<Ciudad>> getciudades() async {
    List<Ciudad> Ciudades = [];
    QuerySnapshot event =
        await FirebaseFirestore.instance.collection('Ciudades').get();
    for (var element in event.docs) {
      Map value = element.data();
      Ciudades.add(new Ciudad(element.id, value['Ciudad']));
    }
    return Ciudades;
  }

  Future<List<Zona>> getzonas(String keyciudad) async {
    List<Zona> Zonas = [];
    // DatabaseEvent event = await FirebaseDatabase.instance.ref('Zonas').once();
    // Map dataMap = (event.snapshot.exists ? event.snapshot.value : {}) as Map;

    QuerySnapshot event = await FirebaseFirestore.instance
        .collection('Zonas')
        .where("Ciudad", isEqualTo: keyciudad)
        .get();
    for (var element in event.docs) {
      Map value = element.data();
      Zonas.add(new Zona(element.id, value['Zona']));
    }
    return Zonas;
  }

  Future<String> getuniquezona(String idzona) async {
    DocumentSnapshot event =
        await FirebaseFirestore.instance.collection('Zonas').doc(idzona).get();
    Map dataMap = event.data();
    return dataMap['Zona'];
  }

  Future<String> getuniqueciudad(String idciudad) async {
    DocumentSnapshot event = await FirebaseFirestore.instance
        .collection('Ciudades')
        .doc(idciudad)
        .get();
    Map dataMap = event.data();
    return dataMap['Ciudad'];
  }

  Future<Ecoemprendedor> ecoemprendedorFuture(String iud) async {
    DocumentSnapshot getdatain =
        await FirebaseFirestore.instance.collection('registro').doc(iud).get();
    Map value = (getdatain.exists ? getdatain.data() : {}) as Map;
    String zonain = await getuniquezona(value['zona']);
    String cuidadin = await getuniqueciudad(value['ciudad']);
    return Ecoemprendedor(
        value['nombre'],
        value['uidUser'],
        cuidadin,
        zonain,
        value['celular'],
        value['correo'],
        value['residuos'],
        value['horarios'],
        value['capacidad'],
        value['detalles'],
        value['direccion'],
        value['descripcion']);
  }

  Future<Recicladora> getRecicladoraFuture(String iud) async {
    DocumentSnapshot getdatain =
        await FirebaseFirestore.instance.collection('registro').doc(iud).get();
    Map value = (getdatain.exists ? getdatain.data() : {}) as Map;

    String zonain = await getuniquezona(value['zona']);
    String cuidadin = await getuniqueciudad(value['departamento']);
    return Recicladora(
        value['nombre'],
        value['uidUser'],
        cuidadin,
        zonain,
        value['celular'],
        value['correo'],
        value['dias'],
        value['horarios'],
        value['residuos'],
        value['ruta'],
        value['detalles']);
  }

  Stream<String> getNombreciudadStream(String idciudad) async* {
    String nombreciudad;
    try {
      yield nombreciudad;
      nombreciudad = await getuniqueciudad(idciudad);
      yield nombreciudad != null &&
              nombreciudad.isNotEmpty &&
              nombreciudad.length > 0
          ? nombreciudad
          : null;
    } catch (e) {
      print('------------------------eeeeee---------------------------');
      print(e);
      yield null;
    }
  }
}
