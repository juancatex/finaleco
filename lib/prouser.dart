import 'package:firebase_auth/firebase_auth.dart';

class Prouser {
  final FirebaseAuth _auth;
  Prouser(this._auth);
  Stream<User> get authState => _auth.userChanges();
}
