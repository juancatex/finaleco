import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_kit_flutter/constants/Theme.dart';
import '../services/crud_services.dart';
import 'drawer-tile.dart';

class MaterialDrawer extends StatelessWidget {
  final Map userdatain;
  final user = FirebaseAuth.instance.currentUser;
  CRUDServices validador = new CRUDServices();
  MaterialDrawer({this.userdatain});
  Future salir(context) async {
    await FirebaseAuth.instance.signOut();
    Fluttertoast.showToast(
        msg: "Sesión cerrada",
        textColor: Color.fromARGB(255, 254, 249, 249),
        backgroundColor: Color.fromARGB(255, 83, 80, 80));
  }

  List<Widget> datouser = [];
  void getdata() {
    datouser = [];
    if (user.email != null) {
      datouser.add(Text(
        user?.email.toString(),
        style: TextStyle(color: Colors.white, fontSize: 13),
      ));
    }

    if (user.displayName != null && user.displayName.isNotEmpty) {
      datouser.add(SizedBox(
        height: 5,
      ));
      datouser.add(Text(
        user?.displayName.toString(),
        style: TextStyle(color: Colors.white, fontSize: 11),
      ));
    }
    print(datouser);
  }

  @override
  Widget build(BuildContext context) {
    getdata();
    return Drawer(
      child: Column(children: [
        SizedBox(
          height: 210,
          child: DrawerHeader(
              decoration:
                  BoxDecoration(color: Color.fromARGB(255, 10, 126, 74)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Image(
                        image: AssetImage('assets/img/drawer.png'),
                        height: 110,
                        fit: BoxFit.fitHeight),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        maxRadius: 15,
                        minRadius: 15,
                        backgroundImage: user?.photoURL.toString() != 'null'
                            ? NetworkImage(user.photoURL.toString())
                            : AssetImage('assets/img/profile-user.png'),
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      // _nameUser(context)

                      Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: datouser)
                    ],
                  ),
                ],
              )),
        ),
        Expanded(
            child: ListView(
          padding: EdgeInsets.only(top: 8, left: 8, right: 8),
          children: [
            DrawerTile(
                icon: Icons.home,
                onTap: () {
                  Navigator.pop(context);
                },
                iconColor: MaterialColors.mysecondary,
                title: "Inicio"),
            (userdatain != null && userdatain.containsKey('rol'))
                ? DrawerTile(
                    icon: Icons.handyman,
                    onTap: () {
                      switch (userdatain['rol']) {
                        case 'Ecoemprendedor':
                          Navigator.pushNamed(context, '/formecoempren');
                          break;
                        case 'Reciclador':
                          Navigator.pushNamed(context, '/formrecicladora');
                          break;
                      }
                    },
                    iconColor: MaterialColors.mysecondary,
                    title: "Edita tu servicio",
                  )
                : DrawerTile(
                    icon: Icons.handyman,
                    onTap: () {
                      Navigator.pushNamed(context, '/options');
                    },
                    iconColor: MaterialColors.mysecondary,
                    title: "Registrar Servicio"),
            DrawerTile(
              icon: Icons.book,
              onTap: () {
                Navigator.pushNamed(context, '/normativas');
              },
              iconColor: MaterialColors.mysecondary,
              title: "Normativa ambiental",
            ),
            DrawerTile(
              icon: Icons.video_library,
              onTap: () {
                Navigator.pushNamed(context, '/videos');
              },
              iconColor: MaterialColors.mysecondary,
              title: "Videos",
            ),
            DrawerTile(
              icon: Icons.menu_book,
              onTap: () {
                Navigator.pushNamed(context, '/manual');
              },
              iconColor: MaterialColors.mysecondary,
              title: "Manual de Usuario",
            ),
            DrawerTile(
              icon: Icons.info_outline_rounded,
              onTap: () {
                Navigator.pushNamed(context, '/acercade');
              },
              iconColor: MaterialColors.mysecondary,
              title: "Acerca de Redcicla",
            ),
            DrawerTile(
              icon: Icons.logout,
              onTap: () {
                salir(context);
              },
              iconColor: MaterialColors.mysecondary,
              title: "Cerrar sesión",
            ),
          ],
        )),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(children: [
                        Image(
                            image: AssetImage('assets/img/cruz.png'),
                            height: 40,
                            fit: BoxFit.fitWidth)
                      ]),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Schweizerische Eidgenossenschaft',
                            style: TextStyle(
                                color: Color.fromARGB(255, 125, 124, 124),
                                fontSize: 10)),
                        Text('Confédération suisse',
                            style: TextStyle(
                                color: Color.fromARGB(255, 125, 124, 124),
                                fontSize: 10)),
                        Text('confederazione Svizzera',
                            style: TextStyle(
                                color: Color.fromARGB(255, 125, 124, 124),
                                fontSize: 10)),
                        Text('confederaziun svizra',
                            style: TextStyle(
                                color: Color.fromARGB(255, 125, 124, 124),
                                fontSize: 10)),
                        SizedBox(
                          height: 10,
                        ),
                        Text('Embajada de Suiza',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 10)),
                        Text('Cooperación Suiza en Bolivia',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 10)),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        )
      ]),
    );
  }
}
