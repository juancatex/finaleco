import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'dashboard/addResiduos.dart';
import 'dashboard/addcity.dart';
import 'dashboard/addzona.dart';
import 'dashboard/ciudad_admin.dart';
import 'dashboard/form_pdf.dart';
import 'dashboard/formvideos.dart';
import 'dashboard/pdfs_admin.dart';
import 'dashboard/residuos_admin.dart';
import 'dashboard/update_ciudad.dart';
import 'dashboard/update_pdf.dart';
import 'dashboard/update_residuo.dart';
import 'dashboard/update_video.dart';
import 'dashboard/update_zona.dart';
import 'dashboard/users_admin.dart';
import 'dashboard/videos_admin.dart';
import 'dashboard/zona_admin.dart';
import 'firebase_options.dart';
import 'prouser.dart';
import 'screens/acercade.dart';
import 'screens/details_ecoprendedor.dart';
import 'screens/details_recicladora.dart';
import 'screens/emprendimiento.dart';
import 'screens/form_ecoemprendimiento.dart';
import 'screens/form_recicladora.dart';
import 'screens/login_page.dart';
import 'screens/manual.dart';
import 'screens/normativas.dart';
import 'screens/onboarding.dart';
import 'screens/options.dart';
import 'screens/recicladorabase.dart';
import 'screens/registration_screen.dart';
import 'screens/videos.dart';
import 'wait.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(Myapp());
}

class Myapp extends StatefulWidget {
  const Myapp({Key key}) : super(key: key);

  @override
  State<Myapp> createState() => _MyappState();
}

class _MyappState extends State<Myapp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (_) => Prouser(FirebaseAuth.instance)),
        StreamProvider(
            create: (context) => context.read<Prouser>().authState,
            initialData: null)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: validador(),
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        routes: {
          "/login": (context) => LoginScreenMain(),
          "/init": (context) => validador(),
          "/acercade": (context) => AcercaDePage(),
          "/videos": (context) => VideosRedciclaPage(),
          "/recicladora": (context) => RecicladoraBasePage(),
          "/ecoempren": (context) => EcoEmprendimientoPage(),
          "/normativas": (context) => NormativasRedciclaPage(),
          "/manual": (context) => Manual(),
          "/options": (context) => OptionsPage(),
          "/formrecicladora": (context) => RegisterRecicladora(),
          "/formecoempren": (context) => RegisterEcoemprendimiento(),
          "/details": (context) => DetailsPage(),
          "/detailsEco": (context) => DetailsPageEco(),
          "/pdfadmin": (context) => PDFs(),
          "/Ciudadesadmin": (context) => Ciudades(),
          "/Zonasadmin": (context) => Zonas(),
          "/Residuosadmin": (context) => Residuos(),
          "/formpdf": (context) => FormPDFs(),
          "/videoadmin": (context) => VideosAdmin(),
          "/UpdatePdf": (context) => UpdatePdf(),
          "/UpdateCiudad": (context) => UpdateCiudad(),
          "/UpdateResiduo": (context) => UpdateResiduo(),
          "/UpdateZona": (context) => UpdateZona(),
          "/UpdateVideo": (context) => UpdateVideo(),
          "/FormVideo": (context) => FormVideo(),
          "/toAddCity": (context) => AddCity(),
          "/toAddZona": (context) => AddZona(),
          "/toAddResiduo": (context) => AddResiduo(),
          "/toRegistrationScreen": (context) => RegistrationScreen(),
          "/useradmin": (context) => Users()
        },
        title: "RedCiclapp",
      ),
    );
  }
}

class validador extends StatelessWidget {
  const validador({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firebaseuser = context.watch<User>();
    if (firebaseuser != null) {
      return Wait();
    }
    return Onboarding();
  }
}
