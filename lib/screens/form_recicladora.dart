import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:material_kit_flutter/constants/Theme.dart';
import 'package:material_kit_flutter/services/crud_services.dart';

import '../model/ciudad_model.dart';
import '../model/global_model.dart';
import '../model/zona_model.dart';

class RegisterRecicladora extends StatefulWidget {
  final Globalmodel userindata;
  RegisterRecicladora({Key key, this.userindata}) : super(key: key);

  @override
  State<RegisterRecicladora> createState() => _RegisterRecicladoraState();
}

class _RegisterRecicladoraState extends State<RegisterRecicladora> {
  final TextEditingController _nombreCompletoController =
      TextEditingController();
  final TextEditingController _numeroCelularController =
      TextEditingController();
  final TextEditingController _queResiduosController = TextEditingController();
  final TextEditingController _callesRecorreController =
      TextEditingController();
  final TextEditingController _horariosRecolectaController =
      TextEditingController();
  final TextEditingController _queDiasRecorreController =
      TextEditingController();
  final TextEditingController _detallesController = TextEditingController();

  final GlobalKey<FormState> _formRecicladora = GlobalKey<FormState>();
  final userFirebase = FirebaseAuth.instance.currentUser;
  final _listacopioservice = CRUDServices();
  bool updatevalues;
  Ciudad comboCiudad;
  List<Ciudad> _itemCiudades = [];
  Zona _comboZona;
  List<Zona> _itemZonas = [];
  Ciudad getciudad(String id) {
    int trendIndex = _itemCiudades.indexWhere((f) => f.key == id);
    return _itemCiudades[trendIndex];
  }

  Zona getzona(String id) {
    int trendIndex = _itemZonas.indexWhere((f) => f.key == id);
    return _itemZonas[trendIndex];
  }

  String hoy = DateTime.now().toString();
  Future verifica() async {
    CRUDServices validador = new CRUDServices();
    Map snapshott = await validador.esValueRol(widget.userindata != null
        ? widget.userindata.uid
        : userFirebase.uid.toString());
    if ((snapshott.containsKey('rol'))) {
      _itemCiudades = await _listacopioservice.getciudades();
      var comboCiudadin = getciudad(snapshott['departamento']);
      _itemZonas = await _listacopioservice.getzonas(comboCiudadin.key);
      setState(() {
        updatevalues = true;
        comboCiudad = comboCiudadin;
        _comboZona = getzona(snapshott['zona']);
        _nombreCompletoController.value =
            TextEditingValue(text: snapshott['nombre']);
        _numeroCelularController.value =
            TextEditingValue(text: snapshott['celular']);
        _horariosRecolectaController.value =
            TextEditingValue(text: snapshott['horarios']);
        _queResiduosController.value =
            TextEditingValue(text: snapshott['residuos']);
        _callesRecorreController.value =
            TextEditingValue(text: snapshott['ruta']);
        _queDiasRecorreController.value =
            TextEditingValue(text: snapshott['dias']);
        _detallesController.value =
            TextEditingValue(text: snapshott['detalles']);
      });
    } else {
      setState(() {
        updatevalues = false;
      });
      initvalues();
    }
  }

  void initvalues() {
    _listacopioservice.getciudades().then((value) {
      setState(() {
        _itemCiudades = value;
      });
    });
  }

  void initvalueszona(String key) {
    _listacopioservice.getzonas(key).then((value) {
      setState(() {
        _itemZonas = value;
      });
    });
  }

  void setzonas() {
    _comboZona = null;
    _itemZonas = [];

    if (comboCiudad != null && comboCiudad.nombre.isNotEmpty) {
      initvalueszona(comboCiudad.key);
    }
  }

  @override
  initState() {
    super.initState();
    verifica();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(updatevalues != null
              ? (!updatevalues
                  ? "Añadir Reciclador/a"
                  : "Modificar Reciclador/a")
              : ''),
          backgroundColor: MaterialColors.myprimary,
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        // key: _scaffoldKey,
        body: updatevalues != null
            ? formularioRecicladora(
                widget.userindata != null
                    ? widget.userindata.email
                    : userFirebase.email.toString(),
                widget.userindata != null
                    ? widget.userindata.uid
                    : userFirebase.uid.toString())
            : Center(
                child: SingleChildScrollView(
                child: Column(
                  children: [
                    CircularProgressIndicator(),
                  ],
                ),
              )));
  }

  Widget formularioRecicladora(String email, String usuarioUid) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Form(
          key: _formRecicladora,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _nombreCompleto(),
              SizedBox(
                height: 15.0,
              ),
              _elegirCiudad(),
              SizedBox(
                height: 15.0,
              ),
              _elegirZona(),
              SizedBox(
                height: 10.0,
              ),
              _numCelular(),
              SizedBox(
                height: 10.0,
              ),
              _queResiduos(),
              SizedBox(
                height: 10.0,
              ),
              _callesRecorre(),
              SizedBox(
                height: 10.0,
              ),
              _queHorarios(),
              SizedBox(
                height: 10.0,
              ),
              _queDias(),
              SizedBox(
                height: 10.0,
              ),
              _queDetalles(),
              SizedBox(
                height: 25.0,
              ),
              updatevalues
                  ? _updateButton(usuarioUid)
                  : _registerButton(email, usuarioUid),
              SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _nombreCompleto() {
    return TextFormField(
        controller: _nombreCompletoController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.account_circle,
            color: MaterialColors.mysecondary,
          ),
          helperText: 'Nombre completo',
          labelText: 'Nombre del reciclador/a',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validatorNombreCompleto(value)));
  }

  Widget _elegirCiudad() {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          padding: EdgeInsets.only(left: 28.0),
          margin: EdgeInsets.only(left: 13.0, right: 0),
          child: DropdownButtonFormField(
            isExpanded: true,
            hint: Text(
              'Elegir ciudad',
              style: TextStyle(color: MaterialColors.mysecondary),
            ),
            items: _itemCiudades.map((valueItem) {
              return DropdownMenuItem(
                value: valueItem,
                child: Text(valueItem.nombre),
              );
            }).toList(),
            value: comboCiudad,
            onChanged: (value) {
              setState(() {
                comboCiudad = value;
                setzonas();
              });
            },
            validator: (value) {
              if (value == null) {
                return 'Debe seleccionar una ciudad';
              }
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 13.0,
          ),
          //margin: EdgeInsets.only(top: 80.0, left: 28.0),
          child: Icon(
            Icons.place,
            color: MaterialColors.mysecondary,
            size: 25.0,
          ),
        ),
      ],
    );
  }

  Widget _elegirZona() {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          padding: EdgeInsets.only(left: 28.0),
          margin: EdgeInsets.only(left: 13.0, right: 0),
          child: DropdownButtonFormField(
            isExpanded: true,
            hint: Text(
              'Elegir zona',
              style: TextStyle(color: MaterialColors.mysecondary),
            ),
            items: _itemZonas.map((valueItem) {
              return DropdownMenuItem(
                value: valueItem,
                child: Text(valueItem.nombre),
              );
            }).toList(),
            value: _comboZona,
            onChanged: (value) {
              setState(() {
                _comboZona = value;
              });
            },
            validator: (value) {
              if (value == null) {
                return 'Debe seleccionar una zona';
              }
            },
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 13.0,
          ),
          //margin: EdgeInsets.only(top: 80.0, left: 28.0),
          child: Icon(
            Icons.location_city,
            color: MaterialColors.mysecondary,
            size: 25.0,
          ),
        ),
      ],
    );
  }

  Widget _numCelular() {
    return TextFormField(
        maxLength: 8,
        keyboardType: TextInputType.number,
        controller: _numeroCelularController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.phone_android,
            color: MaterialColors.mysecondary,
          ),
          helperText: 'Solo numeros, sin agregar +591',
          labelText: 'Nro. de celular',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validatorNumCelular(value)));
  }

  Widget _queResiduos() {
    return TextFormField(
        onTap: () {
          MaterialColors.openmodalResiduos(_queResiduosController, context,
              (values) {
            setState(() {
              _queResiduosController.value = TextEditingValue(text: values);
            });
          });
        },
        readOnly: true,
        controller: _queResiduosController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.article,
            color: MaterialColors.mysecondary,
          ),
          helperText: 'Los 3 residuos principales que recolecta',
          labelText: 'Que residuos recolecta?',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validadorReciduos(value)));
  }

  Widget _callesRecorre() {
    return TextFormField(
        controller: _callesRecorreController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.place,
            color: MaterialColors.mysecondary,
          ),
          helperText: 'Las 2 calles principales que recorre',
          labelText: 'Calles que recorre',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validatorNombreCompleto(value)));
  }

  Widget _queHorarios() {
    return TextFormField(
        onTap: () async {
          TextEditingValue resutlado =
              await MaterialColors.OntapinputfieldTimer(
                  _horariosRecolectaController, context);
          setState(() {
            _horariosRecolectaController.value = resutlado;
          });
        },
        readOnly: true,
        controller: _horariosRecolectaController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.timer_outlined,
            color: MaterialColors.mysecondary,
          ),
          helperText: 'De que hora a que hora?',
          labelText: 'En que horarios recolecta?',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validatorNombreCompleto(value)));
  }

  Widget _queDias() {
    return TextFormField(
        onTap: () {
          MaterialColors.openmodalDiasSemana(_queDiasRecorreController, context,
              (values) {
            setState(() {
              _queDiasRecorreController.value = TextEditingValue(text: values);
            });
          });
        },
        readOnly: true,
        controller: _queDiasRecorreController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.calendar_today,
            color: MaterialColors.mysecondary,
          ),
          //helperText: '',
          labelText: 'Que dias de la semana recorre su ruta?',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validatorNombreCompleto(value)));
  }

  Widget _queDetalles() {
    return TextFormField(
        controller: _detallesController,
        decoration: const InputDecoration(
          icon: Icon(
            Icons.message,
            color: MaterialColors.mysecondary,
          ),
          helperText:
              'Cualquier detalle que considere relevante sobre esta recicladora',
          labelText: 'Detalles adicionales',
          labelStyle: TextStyle(color: MaterialColors.mysecondary),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
          border: UnderlineInputBorder(
              borderSide: BorderSide(color: MaterialColors.mysecondary)),
        ),
        validator: ((value) => _validatorNombreCompleto(value)));
  }

  Widget _registerButton(String email, String usuarioUid) {
    var _em = email;
    return Center(
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: MaterialColors.mysecondary,
            minimumSize: const Size(180.0, 40.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: () async {
            if (_formRecicladora.currentState.validate()) {
              bool resp = await CRUDServices().guardarData({
                'nombre': _nombreCompletoController.text,
                'departamento': comboCiudad.key,
                'zona': _comboZona.key,
                'fecha': hoy,
                'celular': _numeroCelularController.text,
                'horarios': _horariosRecolectaController.text,
                'residuos': _queResiduosController.text,
                'ruta': _callesRecorreController.text,
                'dias': _queDiasRecorreController.text,
                'correo': _em,
                'detalles': _detallesController.text,
                'rol': 'Reciclador',
                'uidUser': usuarioUid
              }, "registro", usuarioUid);

              if (resp) {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('Recicladora registrada'),
                  backgroundColor: Colors.green,
                ));

                widget.userindata != null
                    ? Navigator.pop(context)
                    : Navigator.pushNamedAndRemoveUntil(
                        context, '/init', (route) => false);
              } else {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('Algo salio mal'),
                  backgroundColor: Colors.red,
                ));
              }
            }
          },
          child: Text('Guardar', style: TextStyle(color: Colors.white))),
    );
  }

  Widget _updateButton(String usuarioUid) {
    return Center(
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: MaterialColors.mysecondary,
            minimumSize: const Size(180.0, 40.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
          onPressed: () async {
            if (_formRecicladora.currentState.validate()) {
              bool resp = await CRUDServices().modificarData({
                'nombre': _nombreCompletoController.text,
                'departamento': comboCiudad.key,
                'zona': _comboZona.key,
                'fecha': hoy,
                'celular': _numeroCelularController.text,
                'horarios': _horariosRecolectaController.text,
                'residuos': _queResiduosController.text,
                'ruta': _callesRecorreController.text,
                'dias': _queDiasRecorreController.text,
                'detalles': _detallesController.text
              }, "registro", usuarioUid);

              if (resp) {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('Recicladora actualizada'),
                  backgroundColor: Colors.green,
                ));
                widget.userindata != null
                    ? Navigator.pop(context)
                    : Navigator.pushNamedAndRemoveUntil(
                        context, '/init', (route) => false);
              } else {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text('Algo salio mal'),
                  backgroundColor: Colors.red,
                ));
              }
            }
          },
          child: Text('Modificar', style: TextStyle(color: Colors.white))),
    );
  }

  String _validatorNombreCompleto(String value) {
    if (!_minLength(value)) {
      return 'Por favor llene este campo';
    }
  }

  String _validatorNumCelular(String value) {
    if (!_minLengthCelular(value)) {
      return 'Por favor ingresa solo 8 digitos de celular';
    }
  }

  bool _minLength(String value) {
    return value.isNotEmpty && value.length >= 4;
  }

  bool _minLengthCelular(String value) {
    return value.isNotEmpty && value.length == 8;
  }

  String _validadorReciduos(String value) {
    if (!(value != null && value.isNotEmpty && value.split(',').length == 3)) {
      return 'Por favor debe seleccionar 3 items';
    }
  }
}
