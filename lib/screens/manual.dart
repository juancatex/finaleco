import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../constants/Theme.dart';

class Manual extends StatefulWidget {
  const Manual({Key key});

  @override
  State<Manual> createState() => _ManualState();
}

class _ManualState extends State<Manual> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Manual de usuario"),
          backgroundColor: MaterialColors.myprimary,
        ),
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        body: SfPdfViewer.network(
          'https://firebasestorage.googleapis.com/v0/b/redciclapp-60ddb.appspot.com/o/MANUAL%20DE%20USUARIO.pdf?alt=media&token=bfe9f7cb-bd32-418a-a347-0b8c96d2ab01',
          key: _pdfViewerKey,
        ));
  }
}
