import 'package:flutter/material.dart';
import 'package:material_kit_flutter/constants/Theme.dart';

import '../model/eco_model.dart';
import '../model/ciudad_model.dart';
import '../model/zona_model.dart';
import '../services/crud_services.dart';

class EcoEmprendimientoPage extends StatefulWidget {
  const EcoEmprendimientoPage({Key key}) : super(key: key);

  @override
  State<EcoEmprendimientoPage> createState() => _EcoEmprendimientoPageState();
}

class _EcoEmprendimientoPageState extends State<EcoEmprendimientoPage> {
  Ciudad comboCiudad = new Ciudad('123456789', 'Todas');
  List<Ciudad> itemCiudades = [];
  Zona comboZona = new Zona('987654321', 'Todas');
  List<Zona> itemZonas = [];
  TextEditingController searchController = TextEditingController();
  final _listacopioservice = CRUDServices();
  Ciudad getciudad(String id) {
    int trendIndex = itemCiudades.indexWhere((f) => f.key == id);
    return itemCiudades[trendIndex];
  }

  Stream<List<Ecoemprendedor>> listaemprendedor;
  void initvalues() {
    listaemprendedor = _listacopioservice.ecoemprendedorStream('', null, null);
    _listacopioservice.getciudades().then((value) {
      setState(() {
        itemCiudades = value;
        itemCiudades.add(comboCiudad);
      });
    });
  }

  void initvalueszona(String key) {
    _listacopioservice.getzonas(key).then((value) {
      comboZona = new Zona('987654321', 'Todas');
      setState(() {
        itemZonas = value;
        itemZonas.add(comboZona);
      });
    });
  }

  void setzonas() {
    itemZonas = [];
    comboZona = null;
    if (comboCiudad != null &&
        comboCiudad.nombre.isNotEmpty &&
        comboCiudad.key != "123456789") {
      initvalueszona(comboCiudad.key);
    }
  }

  @override
  void initState() {
    super.initState();
    initvalues();
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Eco-emprendimientos"),
        backgroundColor: MaterialColors.myprimary,
      ),
      backgroundColor: Color.fromARGB(255, 255, 255, 255),
      // key: _scaffoldKey,
      body: Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Image(
                  image: AssetImage('assets/img/ecoemprendimiento 2.png'),
                  height: 130,
                ),
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text('Eco-emprendimientos',
                        style: TextStyle(
                            fontSize: 27.0,
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 117, 117, 117))),
                  )
                ],
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                        width: 280,
                        padding: EdgeInsets.only(left: 16.0, right: 16.0),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Color.fromARGB(255, 0, 0, 0), width: 1),
                            borderRadius: BorderRadius.circular(30)),
                        child: TextField(
                          controller: searchController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Buscar",
                              suffixIcon: IconButton(
                                icon: Icon(Icons.search, color: Colors.black),
                                onPressed: () {
                                  setState(() {
                                    listaemprendedor =
                                        _listacopioservice.ecoemprendedorStream(
                                            searchController.text,
                                            comboCiudad,
                                            comboZona);
                                    FocusManager.instance.primaryFocus
                                        ?.unfocus();
                                  });
                                },
                              ),
                              hintStyle: TextStyle(fontSize: 16.0)),
                        )),
                  )
                ],
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      width: 280,
                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: MaterialColors.mysecondary, width: 1),
                          borderRadius: BorderRadius.circular(15)),
                      child: DropdownButton(
                        underline: SizedBox(),
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: MaterialColors.mysecondary,
                        ),
                        isExpanded: true,
                        hint: Text('Elegir ciudad'),
                        value: comboCiudad,
                        onChanged: (newValue) {
                          setState(() {
                            comboCiudad = newValue;
                            setzonas();
                            listaemprendedor =
                                _listacopioservice.ecoemprendedorStream(
                                    searchController.text,
                                    comboCiudad,
                                    comboZona);
                          });
                        },
                        items: itemCiudades.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem.nombre),
                          );
                        }).toList(),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      width: 280,
                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: MaterialColors.mysecondary, width: 1),
                          borderRadius: BorderRadius.circular(15)),
                      child: DropdownButton(
                        underline: SizedBox(),
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: MaterialColors.mysecondary,
                        ),
                        isExpanded: true,
                        hint: Text('Elegir una zona'),
                        value: comboZona,
                        onChanged: (newValue) {
                          setState(() {
                            comboZona = newValue;
                            listaemprendedor =
                                _listacopioservice.ecoemprendedorStream(
                                    searchController.text,
                                    comboCiudad,
                                    comboZona);
                          });
                        },
                        items: itemZonas.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem.nombre),
                          );
                        }).toList(),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 20.0),
              Center(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      StreamBuilder<List<Ecoemprendedor>>(
                        stream: listaemprendedor,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data.length > 0) {
                              return ListView.builder(
                                shrinkWrap: true,
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, index) {
                                  var r = snapshot.data[index];

                                  return TextButton(
                                      style: TextButton.styleFrom(
                                          backgroundColor:
                                              Color.fromARGB(0, 255, 255, 255)),
                                      onPressed: () {
                                        Navigator.pushNamed(
                                            context, '/detailsEco',
                                            arguments: r);
                                      },
                                      child: Card(
                                        color:
                                            Color.fromARGB(255, 211, 211, 211),
                                        child: Padding(
                                          padding: EdgeInsets.all(10.0),
                                          child: Row(
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.all(12.0),
                                                child: CircleAvatar(
                                                    child: Image(
                                                  image: AssetImage(
                                                      'assets/img/ecoemprendimiento.png'),
                                                )),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    r.nombre,
                                                    style: TextStyle(
                                                        fontSize: 18.0,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        color: Color.fromARGB(
                                                            255, 47, 47, 47)),
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        r.ciudad + " - ",
                                                      ),
                                                      Text(r.zona)
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ));
                                },
                              );
                            } else {
                              return Center(child: CircularProgressIndicator());
                            }
                          } else {
                            return Text(
                              'No hay Registros...',
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black),
                            );
                          }
                        },
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),

      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.home,
          color: Colors.white,
        ),
        backgroundColor: MaterialColors.myprimary,
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
