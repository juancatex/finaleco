class Ecoemprendedor {
  String key;
  String nombre;
  String ciudad;
  String zona;
  String celular;
  String correo;
  String residuos;
  String horarios;
  String capacidad;
  String detalles;
  String direccion;
  String descripcion;

  Ecoemprendedor(
      this.nombre,
      this.key,
      this.ciudad,
      this.zona,
      this.celular,
      this.correo,
      this.residuos,
      this.horarios,
      this.capacidad,
      this.detalles,
      this.direccion,
      this.descripcion);
}
