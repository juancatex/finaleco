class Acopio {
  String key;
  String nombre;
  String ciudad;
  String zona;
  String celular;
  String correo;
  String horarios;
  String direccion;
  String residuos;
  String detalles;

  Acopio(this.nombre, this.key, this.ciudad, this.zona, this.celular,
      this.correo, this.horarios, this.direccion, this.residuos, this.detalles);
}
