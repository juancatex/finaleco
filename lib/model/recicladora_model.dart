class Recicladora {
  String key;
  String nombre;
  String departamento;
  String zona;
  String celular;
  String correo;
  String dias;
  String horarios;
  String residuos;
  String ruta;
  String detalle;

  Recicladora(
      this.nombre,
      this.key,
      this.departamento,
      this.zona,
      this.celular,
      this.correo,
      this.dias,
      this.horarios,
      this.residuos,
      this.ruta,
      this.detalle);
}
